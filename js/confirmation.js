//on recupere les donnees saisies dans le localstorage
let contact  = JSON.parse(localStorage.getItem('contact')) 
let products = JSON.parse(localStorage.getItem('products'))
let total = localStorage.getItem('total') 
//envoie des donnees a l api
fetch('http://localhost:3000/api/cameras/order',{
      method: 'POST',
      body: JSON.stringify({contact, products}),
      headers:{
        "Content-type": "application/json",
      }
})
//affichage de la reponse de l api
      .then(response => response.json())
      .then(response =>{
            console.log(response);
            
            let commande = document.createElement('div')
            commande.classList.add('row-mt-5');
            commande.innerHTML = (`
            
                <h4 class="border border-primary text-center">votre numero de commande : ${response.orderId} </h4>
                </br>
                </br>
                <h4 class="border border-primary text-center">Total de votre commande : ${total/100} €</h4>
            
                
            `)
            document.querySelector('.container').appendChild(commande);
      })
      
      .catch(error => alert('desole nous n avons pas pu faire suite a votre commande'))

      localStorage.clear()
