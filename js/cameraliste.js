//appel de l api pour recuperer les cameras
fetch('http://localhost:3000/api/cameras/')
    .then(res => res.json())
    .then((data) => {
//on affiche chaque camera dans une carte      
      data.forEach(cam => {
            let card = document.createElement('div');
            card.classList.add('card-ms-3');
            card.style.width = "18rem"
//on recupere chaque element de la camera et on envoie l id dans l url de la page produit            
            card.innerHTML = (
                `<img class="img-fluid" src="${cam.imageUrl}" width="100%" height="auto" class="card-img-top" alt="camera">
                <div class="card-body">
                <h5 class="card-title bg-primary ">${cam.name}</h5>
                <p class="prix">prix ${cam.price/100} €</p>                
                <a href="produit.html?id=${cam._id} " class="btn btn-primary">selectionner</a>
                </div>`
            )
            document.querySelector('.row').appendChild(card);
      });
  
    }) 
//on affiche un message d erreur a l utilisateur en cas d erreur de l api        
    .catch(err => alert('desole nous reparons cette erreur au plus vite'))

 
 
  
  
    
 




 

       

  
